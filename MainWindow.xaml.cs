﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Media;
using Vlc.DotNet.Core;
using Vlc.DotNet.Core.Interops.Signatures.LibVlc.Media;
using Vlc.DotNet.Core.Medias;
using Vlc.DotNet.Wpf;

namespace SABM_VLC
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public VlcControl VlcPlayer = new VlcControl();

        public MainWindow()
        {
            InitializeComponent();
            
            // Add the VLC control to the grid to give it dimensions.
            // However VLC does not paint on its control geometry
            // instead it paints on its "VideoSource" image
            ParentGrid.Children.Add(VlcPlayer);

            // WPF dark magic ahead: run-time interpreted data binding
            // When the VLC video changes (is loaded for example), the grid displays the new video image
            var vlcBinding = new Binding("VideoSource") {Source = VlcPlayer};

            // VLC paints into a WPF image
            var vImage = new Image();
            vImage.SetBinding(Image.SourceProperty, vlcBinding);

            // The WPF image is used by a WPF brush
            var vBrush = new VisualBrush {TileMode = TileMode.None, Stretch = Stretch.Uniform, Visual = vImage};

            // The WPF brush is used by the grid element background
            ParentGrid.Background = vBrush;

            VlcPlayer.Play(new PathMedia(@"C:\Users\Public\Videos\Sample Videos\Wildlife.wmv"));

            //  DispatcherTimer setup
            var dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += VlcPlayerOnPlaying;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Start();
        }

        private void VlcPlayerOnPlaying(object sender, EventArgs e)
        {
            var trackInfos = VlcPlayer.Media.TrackInfos;
            foreach (MediaTrackInfo vlcMediaTrackInfo in trackInfos)
            {
                Debug.WriteLine(vlcMediaTrackInfo.CodecFourcc);
                Debug.WriteLine(vlcMediaTrackInfo.Audio.Rate);
                Debug.WriteLine(vlcMediaTrackInfo.Audio.Channels);
            }
            
            
        }

        private void Window_Closed(object sender, System.EventArgs e)
        {
            VlcContext.CloseAll();
        }

        private void VolumeControl_OnDragCompleted(object sender, DragCompletedEventArgs e)
        {
            VlcPlayer.AudioProperties.Volume = (int)Math.Round(VolumeControl.Value);
            Debug.Write(VlcPlayer.Media.TrackInfos);
        }
    }
}
